import os
import subprocess

if __name__=='__main__':
    demo_dir= raw_input("Enter demo dir: ")
    current_dir= raw_input("Enter current dir: ")
    cmd="./main_script.sh "+demo_dir+" "+current_dir
    subprocess.call(cmd, shell=True) #execute the bash script
    
    os.chdir(current_dir) #switch to the working dir
    
    f_old = open("current.txt", "r")
    f_new = open("demo.txt", "r")
    
    for old,new in zip(f_old,f_new):
            print(old.strip())
            os.rename(old.strip(), new.strip())